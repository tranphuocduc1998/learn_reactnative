import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

class Extendtion extends React.Component {

    render() {
        return (
            <View>
                <Text style={styles.text1}>Hello có nghĩa là xin chào!</Text>
                <Text style={styles.text2}>Goodbye tạm biệt, thì thào Wishper</Text>
                <Text style={styles.text3}></Text>
                <TextInput style={{ borderWidth: 1, textAlign: 'center', }}>
                </TextInput>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    text1: {
        height: 100,
        textAlign: 'left',
        color: 'red',
        backgroundColor: 'black',
        fontSize: 25,
        margin: 20,
    },
    text2: {
        height: 100,
        textAlign: 'center',
        textAlignVertical: 'center',
        color: 'red',
        backgroundColor: 'black',
        fontSize: 25,
        margin: 20,
    },
    text3: {
        height: 100,
        textAlign: 'right',
        textAlignVertical: 'bottom',
        color: 'red',
        backgroundColor: 'black',
        fontSize: 25,
        margin: 20,
    },
});

export { Extendtion }