import React from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity } from 'react-native';

class LoginPage extends React.Component {
    state = { username: '', password: '' };


    render() {
        return (
            <View>
                <Text style={styles.welcome}>Login!</Text>
                <TextInput style={styles.input} placeholder="Username"
                    onChangeText={(username) => this.setState({ username })}
                    value={this.state.username}></TextInput>
                <TextInput style={styles.input} placeholder="Password"
                    onChangeText={(password) => this.setState({ password })}
                    value={this.state.password}
                    secureTextEntry={true}></TextInput>
                <TouchableOpacity style={styles.btnLogin} onPress={this._signin}>
                    <Text style={styles.btnLoginText}>Login</Text>
                </TouchableOpacity>
            </View>
        );
    }
    _signin = async () => {
        alert('Hello ' + this.state.username + ', Welcome to us'+ '\nYour password is ' + this.state.password);
    }
}

const styles = StyleSheet.create({
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    input: {
        margin: 15,
        height: 40,
        padding: 5,
        fontSize: 16,
        borderBottomWidth: 1,
        borderBottomColor: '#428AF8',
    },
    btnLogin: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#428AF8',
        marginLeft: 15,
        marginRight: 15,
        padding: 10,
        height: 50,
    },
    btnLoginText: {
        color: '#ffffff',
        fontWeight: '700',
    },
});



export { LoginPage }