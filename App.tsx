import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { LoginPage } from './Training/Login/state';

export default function App() {
  return (
    <View style={styles.container}>
      <LoginPage></LoginPage>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});
